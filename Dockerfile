FROM ubuntu:24.04

RUN apt-get update; \
    DEBIAN_FRONTEND=noninteractive apt install -y --no-install-recommends \
        build-essential \
        ca-certificates \
        pkg-config \
        meson \
        openssh-client \
        git \
        python3 \
        python3-dev \
        python3-pip \
        python3-ruamel.yaml \
        python3-yaml \
        python3-jinja2 \
        python3-json5 \
        pipx \
        psmisc \
        jq \
        curl \
        wget \
        cpio \
        kmod \
        device-tree-compiler \
        tar \
        xz-utils \
        gcc-aarch64-linux-gnu \
        gcc-arm-linux-gnueabihf \
        bc \
        binutils \
        bison \
        dwarves \
        flex \
        perl-base \
        libssl-dev \
        libelf-dev \
        swig

RUN pip3 install --break-system-packages junit-xml

RUN pip3 install --break-system-packages rfc3987

RUN pip3 install --break-system-packages dtschema

RUN pipx install tuxsuite

RUN pipx ensurepath